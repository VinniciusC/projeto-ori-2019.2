#ifndef FUNCOES_COMUNS_H_INCLUDED
#define FUNCOES_COMUNS_H_INCLUDED

#include <iostream>
#include <fstream>

using namespace std;

void ignorarEspacos(int &i, string input);
int nomeTabela(int &i, string &tabela, string input);
void pegaChave(int &i, string &chave, string input);
bool verificaTabela(fstream &metadados, string tabela, string &linha);
int verificaTipo(int &i, string input);
void tipoRegistro(string auxString,string tipo, int k);
void pegaTipo(string primeiraLinha, string &tipo, int &u);
void criaArquivoTabelaNova();
int verificaNumeroTabelasMetadados();
void verificaMetadados();
int verificaCampo(string nome_arqv, string campo);
bool verificaVazia(string input);
bool verificaHash(string tabela, string campo);
bool verificaArvore(string nome_arq);
bool verificaSeExiste(string tabela, string & nome_arqv);
#endif