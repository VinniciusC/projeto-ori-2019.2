#include "funcoes_comuns.h"

void ignorarEspacos(int &i, string input){
  while(input[i] == ' ') i++;
}

int nomeTabela(int &i, string &tabela, string input){

	if (isalpha(input[i])){	// Verifica se o nome é valido, i.e. começa com um caractere alfabetico


	  while(input[i] != ' ' && input[i] != '\0'){ //Identifica o nome da tabela
	    tabela = tabela + input[i];
	    i++;
	  }
	} else {
		cout <<"O nome digitado não é um nome valido."<<endl;
		return 1;
	}
	return 0;
}

void pegaChave(int &i, string &chave, string input){
  while(input[i]){ //Identifica a chave
    chave = chave + input[i];
    i++;
    }
}


bool verificaTabela(fstream &metadados, string tabela, string &linha){ // Se existe uma tabela com o nome passado, retorna true, se não, false
  int j=0;
  string tabela_cadastrada;
  metadados.seekg(0,metadados.beg);
  while(!metadados.eof()){
    getline(metadados,linha);
    
    if (linha[0]=='I'){ //Ignora a primeira linha que descreve as colunas da tabela
    
    }else if(linha[0]=='*'){ //Caso tenha * a tabela é inválida, já foi excluída

    }else{
          while (linha[j]!='|'){ //Pula a descrição do indice
          j++;
        }
        j++;
        while (linha[j]!='\0'){ //pega o nome da tabela
          tabela_cadastrada = tabela_cadastrada + linha[j];
          j++;
        }
        j=0;
        if(tabela_cadastrada==tabela){
            return true;
        }
        tabela_cadastrada = "";
      }
    }

    return false;
}

// Se existe uma tabela com o nome passado, retorna true, se não, false
// retorna tb o nome do arquivo, ex: 1.dat
// só recebe o nome da tabela em questao e o nome do arqv
bool verificaSeExiste(string tabela, string & nome_arqv) {
    //cout<<"\n__verificaSeExiste__\n";
    int j = 0;
    int doWhile = 1;
    string tabela_cadastrada, linha;
    //cout<< tabela <<"<tabela\n";
    fstream metadados;
    metadados.open("metadados.dat", ios:: in ); //abre em modo leitura, escrita e mantendo as informações já salvas
    metadados.seekg(0, metadados.beg);

    while (!metadados.eof()) {
    //do{
      //  cout << "entrou no while\n" ;
        flag_inicio_verificaSeExiste: getline(metadados, linha);
        //cout << "linha 1 ->"<< linha << "<- linha 1 "<< endl;
        if (linha[0] != 'I' && linha[0] != ' ') { //Ignora a primeira linha das colunas da tabela e a segunda linha em branco

            while (linha[j] != '|') { //Pula a descrição do indice
                nome_arqv = nome_arqv + linha[j];
                j++;
            }
            j++; // pula a | 

            while (linha[j] != '\0' && linha[j] != '\n') { //pega o nome da tabela
                tabela_cadastrada = tabela_cadastrada + linha[j];
                j++;
            }
            j = 0;

            if (tabela_cadastrada == tabela) {
                nome_arqv = nome_arqv + ".dat";
                metadados.close();
                //cout<< nome_arqv << "<nome_arqv\n";
                //cout << "saiu1 no while\n" ;
                return true;
            }
            nome_arqv = "";
            tabela_cadastrada = "";
        }
       
        j = 0;
        goto flag_inicio_verificaSeExiste;
    }
    //cout << "saiu2 no while\n" ;

    metadados.close();
    return false;
}

bool verificaArvore(string nome_arq){ // retorna true se hash existe
    ifstream arquivo;
    arquivo.open("Arvore /" + nome_arq, ios::in);

    if(arquivo){
        return true;
    }else{
        return false;
    }

}

bool verificaHash(string tabela, string campo){ // retorna true se hash existe
    ifstream metadados_index;
    string linha, concatenacao = tabela + "_" + campo;
    metadados_index.open("metadados_index.dat", ios::app);
    while (!metadados_index.eof()){
        cout<<"entrou\n";
      
      getline(metadados_index, linha);
      if(linha.find(concatenacao)!=string::npos){
        metadados_index.close();
        cout<<"saiutrue\n";
        return true;
      }
    }
    metadados_index.close();
    cout<<"saiufalse\n";
    return false;

}
//recebe o input e verifica se os tipos são INT,FLT,STR,BIN e
//se os campos são validos(somente letras)
int verificaTipo(int &i, string input){

  bool aux = false;

	while(input[i] !=(' ')){ // Passa pelo nome da tabela, ate o espaco
    		i++;
    	}
    	i++; //pula o espaco

  while (input[i] != ('\0') ){
    if(input[i] == (':') || input[i] == (';') )
      i++;

    string tipo;
	  while(input[i] != (':') ){ // Passa pelo tipo, salva os caracteres para comparação
    	tipo = tipo+input[i];
    	i++;
    }

    //cout<< tipo <<"__tipo\n";
    // verifica se o tipo é valido
    if ( (tipo != "INT") && (tipo != "FLT") && (tipo != "STR") && (tipo != "BIN") ){
    aux = true ;
    }

    i++; //pula o ':'
    while(input[i] !=(';') && (input[i] != ('\0')) ){ // verifica se o campo é válido(somente letras)
      if ((input[i] >= 'a' && input[i] <= 'z') || (input[i] >= 'A' && input[i] <= 'Z')){
        //cout<< input[i] << "1\n";
        i++;
      }else {
        //cout<< input[i] << "__2\n";
        i++;
        break;
      }
    }

  }
  if (aux == true ){
    return 1;
  } else{
    return 0;}
}

void pegaTipo(string primeiraLinha, string &tipo, int &u){


	  	int k;

	  	string primeiraL;
	  	primeiraL=primeiraLinha;



	  	while (primeiraL[u] != ':'){

	  		tipo=tipo+ primeiraL[u];

	  		u++;

	  	}


}

void tipoRegistro(string auxString,string tipo, int k){



	 	if (tipo == "STR"){

	  		if (!isalpha(auxString[0])){
	  		cout<<"Campo digitado invalido"<<endl;
	  		exit(1);
	  		}


	  	}
	  	int j=0;

	  	if (tipo =="INT"){

	  		while (auxString[j] !='\0'){
	  			if(!isdigit(auxString[j])){
	  				cout<<"Campo digitado invalido"<<endl;
	  				exit(1);
	  			}
	  			j++;
	  		}

	  	}

	  	if (tipo =="FLT"){

	  		while (auxString[j] !='\0'){
	  			if(!isdigit(auxString[j])){

	  				if (auxString[j]!='.'){
		  				cout<<"Campo digitado invalido"<<endl;
		  				exit(1);
		  			}
	  			}
	  			j++;
	  		}
	  	}

	  	if (tipo =="BIN"){

	  		while (auxString[j] !='\0'){
	  			if (!isalpha(auxString[0])){
	  				
	  			
		  				cout<<"Campo digitado invalido78"<<endl;
		  				exit(1);

	  			}
	  			j++;
	  		}
	  	}


	  	return;
}

bool verificaVazia(string input){
	string aux;
	aux = input;
	int i=0;
	if (aux.length()==0){ // Se a string estiver vazia, retorne true
		return true;
		}else {

			while(aux[i] == ' '){
			i++;
			}
			if (aux[i] |= ' '){	// Se o valor em que o while parou for diferente de 'espaço', isso quer dizer que
							// existe algum caractere diferente de espaço, então retorna falso para vazio
				return false;
			}else{			// Se o valor em que o while parou for igual a espaço, então a string só tem espaços.
			return true;
			}
		}

}

/*
 Procedimento
 Nome: verificaMetadados
 Descrição: Essa rotina verifica a existência do arquivo Metadados no repositório.
 Caso não exista, ele cria o arquivo e adiciona o cabeçalho referente ao índice e nome da tabela.
*/

void verificaMetadados(){

    FILE *metadados;
    metadados = fopen("metadados.dat", "r");

        if(metadados == NULL){
            metadados = fopen("metadados.dat", "w");
            fprintf(metadados, "Indice | Nome da Tabela");
            fclose(metadados);
        }

    fclose(metadados);
    
    FILE * file;
    file = fopen("buscas.dat", "r");
    if (file == NULL) {
        file = fopen("buscas.dat", "w");
        fclose(file);
    }
    fclose(file);
}


/*
 Função
 Nome: VerificaNumeroTabelasMetadados
 Descrição: Uma das rotinas de manutenção dos metadados.
 Essa função retorna o número de tabelas existentes no metadados.
*/

int verificaNumeroTabelasMetadados(){

    ifstream metadados;
    string input;
    int NumeroTabelas = 0;

    metadados.open("metadados.dat");

    while(!metadados.eof()){
        getline(metadados,input);
        NumeroTabelas++;
    }

    metadados.close();

    return NumeroTabelas-1;
}


/*
 Procedimento
 Nome: CriaArquivoTabelaNova
 Descrição: Quando é criada uma tabela nova, é também criado um novo arquivo a fim de
 armazenar seus dados.
 */

void criaArquivoTabelaNova(){

    int TabelaAtual = verificaNumeroTabelasMetadados();

}