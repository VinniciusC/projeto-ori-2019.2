#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <queue>
#include <ctype.h>
#include <vector>
#include "funcoes_comuns.h"
#include "comandoCT.h"
#include "comandoRT.h"
#include "comandoIR.h"
#include "sstream"
#include "comandoRR.h"


#define QNT_MAX_REGS_BALDE 510
#define TAM_BALDE 4096

using namespace std;

typedef struct resultado Resultado;
typedef struct tabela Tabela;
typedef struct balde Balde;

struct balde{
    int contador_registros;
    int *valor_registro;
    int *endereco_registro;
    int prox;
    char lixo[4] = {'L','I','X','O'};
};

struct tabela{
    string nome;
    Tabela *prox;
    Resultado *lista_resultados;
};

struct resultado{
    int endereco;
    Resultado *prox;
};

Tabela *inicio = NULL;

Resultado *comeco = NULL;

void insereResultadoLista ( Resultado ** comeco, int x) {
    Resultado* novo = new Resultado;
    Resultado*  p = *comeco;

    novo->endereco = x;
    novo->prox = NULL;

    if (*comeco == NULL)
    {
        *comeco = novo;
        return;
    }

    while ( p->prox != NULL)
         p =  p->prox;

    p->prox = novo;
    return;
}

void insereTabelaLista ( Tabela ** inicio, string nome) {
    Tabela* novo = new Tabela;
    Tabela*  p = *inicio;

    novo->nome = nome;
    novo->prox = NULL;
    novo->lista_resultados = NULL;

    if (*inicio == NULL)
    {
        *inicio = novo;
        return;
    }

    while ( p->prox != NULL ){
        if ( p->nome == nome)
            return;
        p =  p->prox;
    }

    p->prox = novo;
    return;
}

void Atualiza ( Tabela ** inicio, string nome, Resultado ** comeco) {
    Tabela*  p = *inicio;

    while ( p->nome != nome )
         p =  p->prox;

    Resultado *removido =  p->lista_resultados ;
     p->lista_resultados = *comeco;

    free(removido) ;
}

void comandoRI(string input){
  int i = 2, nomeVal;
  string chave, tabela, nome_arqv, endereco;
  fstream arquivo_hash, arquivo_arvore;
  ignorarEspacos(i, input);
  nomeVal = nomeTabela(i,tabela,input);
  if (nomeVal== 0){
	  ignorarEspacos(i, input);
	  pegaChave(i,chave,input);
      verificaSeExiste(tabela,nome_arqv);

      endereco = "Hash/"+nome_arqv;
      arquivo_hash.open(endereco);
      if(arquivo_hash){
          remove(endereco.c_str());
      }

      endereco = "Arvore /"+nome_arqv;
      arquivo_arvore.open(endereco);
      if(arquivo_arvore){
          remove(endereco.c_str());
      }

	  cout << "Indice " << chave << " removido da tabela " << tabela << endl;
  }
}

void comandoAT(string input){ //mostra uma tabela especifica
  int i = 2, nomeVal, j = 0;
  string tabela, endereco_arq, linha, index, endereco;
  ignorarEspacos(i, input);
  fstream achou, metadados, arquivo_arvore, arquivo_hash;
  nomeVal = nomeTabela(i,tabela,input);
  if (nomeVal ==0){
 	 cout << "O comando exibe informações da tabela \n" << tabela << endl;
  }

	metadados.open("metadados.dat", ios::in|ios::out|ios::app); //abre em modo leitura, escrita e mantendo as informações já salvas
  metadados.seekg(0,metadados.beg);

	if(verificaTabela(metadados,tabela,linha)){
		while(linha[j]!='|'){ //pega o indice para mostrar o arquivo da tabela
		endereco_arq+= linha[j];
		j++;
		}
        index = endereco_arq;
		endereco_arq = "Tabelas/" + endereco_arq + ".dat";
		achou.open(endereco_arq);
		while(getline(achou, linha))
            cout << linha;
		cout << endl;

        endereco = "Hash/"+ index + ".dat";
        arquivo_hash.open(endereco);
        if(arquivo_hash){
          cout << "Possui indice hash" << endl;
        }

        endereco = "Arvore /"+ index + ".dat";
        arquivo_arvore.open(endereco);
        if(arquivo_arvore){
          cout << "Possui indice por árvore" << endl;
        }
	}else{
		cout << "Tabela não encontrada!" << endl;
	}
	metadados.close();
	achou.close();
}



void comandoGI(string input){
  int i = 2, nomeVal;
  string chave, tabela;
  ignorarEspacos(i, input);
  nomeVal =nomeTabela(i,tabela,input);
  if (nomeVal == 0){
	  ignorarEspacos(i, input);
	  pegaChave(i,chave,input);
	  cout << "Gera o indice da tabela " << tabela  << " referente a chave " << chave << endl;
  }
}

void comandoLT(string input){ //mostra todas as tabelas criadas
  FILE *arquivo;
  char linha[200];

  arquivo = fopen("metadados.dat", "r");
  while(!feof(arquivo)){
    fgets(linha, 200, arquivo);
    cout << (linha);
    linha[0] = '\0';
  }
  printf("\n");
  fclose(arquivo);

}

//pegaCampo retorna 1 se campo for valido e nao vazio, caso contrario retorna 0 
bool pegaCampo(int & i, string & campo, string input) {
    int flag = 0;
    while (input[i] != (':')) {
        // verifica se o campo é válido(somente letras)
        if ((input[i] >= 'a' && input[i] <= 'z') || (input[i] >= 'A' && input[i] <= 'Z')) {
            campo = campo + input[i];
            // cout<< input[i] << "1Campo\n";
            i++;
            flag++;
        } else {
            // cout<< input[i] << "2Campo\n";
            return 0;
        }
    }
    i++; // para pular o :
    if (flag != 0) {
        return 1;
    } else {
        // cout<< input[i] << "3campo\n";
        return 0;
    }
}

//pegaValor retorna 0 se valor for vazio, retorna 1 se existir
bool pegaValor(int & i, string & valor, string input) {
    int flag = 0;
    while ((input[i] != '\0')) { //Identifica a chave
        valor = valor + input[i];
        // cout<< input[i] << "1valor\n";
        i++;
        flag++;
    }

    if (flag != 0) {
        return 1;
    } else {
        // cout<< input[i] << "3valor\n";
        return 0;
    }
}

//recebe o arquivo com a tabela(nome_arqv) e o nome do campo:
//retorna 0 se nao existir e retorna o numero de ; + 1 
int verificaCampo(string nome_arqv, string campo) { // corretissimo 
     //cout<<"\n__verificaCAMPO__\n";
    fstream arquivo;
    string linha;
    string campo_arqv;
    int j = 0;
    int cont = 0;

   
    arquivo.open(nome_arqv, ios:: in );
    getline(arquivo, linha);
    //cout << endl;
    
    while (1) {
        flag:
      //  cout<<"entrou\n";
            while ((linha[j] != (';')) && (linha[j] != (':')) && (linha[j] != ('\n')) && (linha[j] != '\0')) {
                campo_arqv = campo_arqv + linha[j];
                j++;
            }
        
        if (linha[j] == (':')) {
            campo_arqv = ""; // zera pq vao ser os tipos, ex INT FLT 
            j++; // pula o :
            goto flag;
        }
        if (linha[j] == (';')) {
            cont++;
            j++; // pula o ; 
            if (campo_arqv == campo) {
                arquivo.close();
                return cont;
            } else {
                campo_arqv = "";
                goto flag;
            }
        }

        if ((linha[j] == ('\0')) || (linha[j] = ('\n'))) {
            cont++;
            if (campo_arqv == campo) {
                arquivo.close();
                return cont;
            } else {
                cont = 0;
                break;
            }
        }
    }
    arquivo.close();
    return false;

}






//comando BR
//recebe a linha de comando, retorna sucesso ou erro, 
//quando usar o comando AR, so imprimir esse vetor
//**falta a funcao tabela_Existe e a parte de abrir o arquivo da tabela em questao**

void exibeResultado(string input, Tabela ** inicio, string tabela){
    Tabela*  p = *inicio;
    while ( p->nome != tabela ) 
         p =  p->prox;

    cout<< p->nome << "p->nome:"<<endl;
    Resultado* r = p->lista_resultados;

    int i = 2, j, k = 0, doWhile_interno;
    string  nome_arqv, cabecalho, linha, aux;
    ifstream arquivo,buscas;

    if (verificaSeExiste(tabela, nome_arqv)) {
        nome_arqv = "Tabelas/" + nome_arqv;
        arquivo.open(nome_arqv, ios:: in );
        getline(arquivo, cabecalho);
        do{         
                    arquivo.seekg(r->endereco, ios::beg);
                    getline(arquivo, linha);
                    i = 0 ;
                    j = 0 ; 
                    doWhile_interno = 1 ;
                    do{
                        while ((cabecalho[i] != (':')) && (cabecalho[i] != (';')) && (cabecalho[i] != ('\0'))) {
                            aux = aux + cabecalho[i];
                            i++;}
                        if (cabecalho[i] == (':')) 
                        {
                            aux = "";
                            i++;
                        }
                        if ( (cabecalho[i] == (';')) || (cabecalho[i] == ('\0')) )
                        {
                            cout<<aux << "->" ;
                            if (cabecalho[i] == (';'))
                                i++;
                            
                            aux = "";
                            while (linha[j] != (';'))
                            {
                                aux = aux + linha[j];
                                j++;
                            }
                            cout<< aux << endl;
                            j++;
                            if(linha[j] == ('\0') )
                            {
                                doWhile_interno = 0 ;
                            }
                        }
                    }while(doWhile_interno);
                    cout <<endl;
                    r = r->prox;
        }while( r  != NULL);
    }
}

bool comandoAR(string input) {
    cout<<input<<endl;
    int i = 2 ;
    string tabela;
    ignorarEspacos(i, input);
    nomeTabela(i, tabela, input);

    exibeResultado(input, &inicio, tabela);
    
    return 0;
}

int minha_hash(int chave, int qnt){
    return((chave*47)+500)%qnt;
}

int escreve_balde(fstream& arquivo, int pos, Balde balde){
    int pos_anterior = arquivo.tellg();
    arquivo.seekg(pos, ios::cur);
    
    arquivo<< balde.contador_registros;
    for (int i = 0; i < balde.contador_registros; i ++)
        arquivo<< balde.valor_registro[i];
    for (int i = 0; i < balde.contador_registros; i ++)
        arquivo<< balde.endereco_registro[i];
    for (int i = 0; i < 4; i ++)
        arquivo<< balde.lixo[i];
    arquivo<< balde.prox;


    arquivo.seekg(pos_anterior, ios::cur);

    return (pos + TAM_BALDE);
}

void inicializa(string caminho_arqv, int  quantidade_registros){
    Balde balde;
    int quantidade_baldes, pos;
    fstream arquivo;

    cout<< "------\n" << caminho_arqv << "\n" << quantidade_registros << "\n";


    quantidade_baldes = quantidade_registros/ QNT_MAX_REGS_BALDE ;
    quantidade_baldes = (quantidade_baldes/ 0.75); 
    
    if(quantidade_registros < QNT_MAX_REGS_BALDE)
        quantidade_baldes = 1;

    cout<< quantidade_baldes <<"<qntbaldes\n";
    balde.contador_registros = 0;
    balde.prox = -1;
    for (int i = 0; i < QNT_MAX_REGS_BALDE; i++){
        cout<<"aoba\n";
        balde.valor_registro[i] = -1;
        balde.endereco_registro[i] = -1;
    }
    cout << "sai";
    arquivo.open(caminho_arqv, ios::out | ios::app | ios:: binary); //abre em modo leitura, escrita e mantendo as informações já salvas
    cout << "NABO\n" ;
    if (!(arquivo.is_open())){
        cout << "erro ao abrir o aqrv /indice\n";
    }else{
        cout <<"xablau";
        arquivo << quantidade_baldes;

        pos = arquivo.tellg();

        for (int i = 0; i < quantidade_baldes; i++){
            pos = escreve_balde(arquivo, pos, balde);    
        }

    arquivo.close();
    }

}



void insere_balde(fstream& arquivo, int pos_balde, int valor_reg, int endereco_reg){
    Balde balde;
    string get_line;
    int pos;

    arquivo.seekg(pos_balde, ios::beg);

    getline(arquivo,get_line);
    balde.contador_registros = atoi(get_line.c_str());
    for (int i = 0; i < balde.contador_registros; i ++){
        getline(arquivo,get_line);
        balde.valor_registro[i] = atoi(get_line.c_str());}
    for (int i = 0; i < balde.contador_registros; i ++){
        getline(arquivo,get_line);
        balde.endereco_registro[i] = atoi(get_line.c_str());}
    for (int i = 0; i < 4; i ++){
        getline(arquivo,get_line);
        balde.lixo[i]=get_line[0];}
    getline(arquivo,get_line);
    balde.prox=atoi(get_line.c_str());


    
    // se tem espaco no balde
    if (balde.contador_registros < QNT_MAX_REGS_BALDE){
        balde.valor_registro[balde.contador_registros] = valor_reg ; 
        balde.endereco_registro[balde.contador_registros] = endereco_reg ; 
        balde.contador_registros++;

        escreve_balde(arquivo, pos_balde, balde);
    }
    // se o balde cheio tem extensao 
    else if (balde.prox != -1){
        insere_balde(arquivo, balde.prox, valor_reg, endereco_reg);
    }
    // se ta cheio e n tem extensao
    else{
        arquivo.seekg(0, ios::end);
        pos = arquivo.tellg();
        balde.prox = pos;
        escreve_balde(arquivo, pos_balde, balde);

        balde.contador_registros = 1;
        balde.valor_registro[0] = valor_reg ; 
        balde.endereco_registro[0] = endereco_reg ; 
        for (int i = 1; i < QNT_MAX_REGS_BALDE; i++){
            balde.valor_registro[i] = -1 ; 
            balde.endereco_registro[i] = -1 ; 
        }
        arquivo.seekg(0, ios::end);
        pos = arquivo.tellg();
        escreve_balde(arquivo, pos, balde);
    }
}


void insere(string caminho_arqv, int valor_reg, int endereco_reg){
    fstream arquivo;
    int quantidade_baldes;
    int pos_balde;// para saber em qual balde inserir mais tarde / aonde 

    arquivo.open(caminho_arqv, ios::in | ios:: binary);
    cin >> quantidade_baldes;

    pos_balde = minha_hash(valor_reg, quantidade_baldes) * TAM_BALDE + sizeof(quantidade_baldes);
    insere_balde(arquivo, pos_balde, valor_reg,  endereco_reg);

    arquivo.close();
}


void busca(string caminho_arqv, int valor, string tabela, char modo){
    Resultado *comeco = NULL;
    bool achou = 1;
    int quantidade_baldes;
    int index;
    int pos_balde;
    fstream arquivo;
    string get_line;
    Balde balde;

    arquivo.open(caminho_arqv, ios::in | ios:: binary);
    if (!(arquivo.is_open())){
        cout <<"arqv.is_open\n";
        cout << "Erro ao abrir o arquivo:" + caminho_arqv + "\n";
        exit(1);
    }

    getline(arquivo,get_line);
    quantidade_baldes = atoi(get_line.c_str());

    pos_balde = (minha_hash(valor, quantidade_baldes) * TAM_BALDE) + sizeof(quantidade_baldes);

    arquivo.seekg(pos_balde, ios::beg);
    do{ 
        getline(arquivo,get_line);
        balde.contador_registros = atoi(get_line.c_str());
        for (int i = 0; i < balde.contador_registros; i ++){
            getline(arquivo,get_line);
            balde.valor_registro[i]=atoi(get_line.c_str());}
        for (int i = 0; i < balde.contador_registros; i ++){
            getline(arquivo,get_line);
            balde.endereco_registro[i]=atoi(get_line.c_str());}
        for (int i = 0; i < 4; i ++){
            getline(arquivo,get_line);
            balde.lixo[i]=get_line[i];}

        getline(arquivo,get_line);
        balde.prox=atoi(get_line.c_str());

        pos_balde = balde.prox;
        for (int i = 0; i < balde.contador_registros; i++){
            if (balde.valor_registro[i] == valor){
                insereResultadoLista( &comeco, balde.endereco_registro[i]);
                if(modo == 'U'){
                    achou = 0;
                    break;
                }
            }
        }
    } while (balde.prox != -1 && achou == 1);

    insereTabelaLista(&inicio, tabela);
    Atualiza(&inicio, tabela, &comeco);

    
    arquivo.close();

}

void comandoHASH(string input) { 
    int i = 4, j = 0, k = 0 , contador = 0,cont_barras, endereco_resultado = 0, doWhile, res_hash, verificaint;
    string tabela, campo, valor, linha, aux, nome_arqv, resultado,linha_buscas;
    string caminho_arqv;
    string aux_index;
    int blablabla;
    fstream arquivo, arquivo_hash, metadados_index;
    ignorarEspacos(i, input);
    nomeTabela(i, tabela, input);


    ignorarEspacos(i, input);
    if (pegaCampo(i, campo, input))
        cout << "pegou o campo ->  " << campo <<endl;
    
    if (verificaSeExiste(tabela, nome_arqv)) {
        if(!verificaHash(tabela,campo)){
            string tipo_campo;
            string endereco = "Tabelas/" + nome_arqv;
            cont_barras = verificaCampo(endereco, campo); 
    
            arquivo.open(endereco, ios:: in ); //  o nome do arquivo  é 1,2,...,x.dat
            getline(arquivo, linha);
            verificaint = linha.find(campo); // verifica se o campo que vai ser indexado eh  int

            tipo_campo = (linha.substr((verificaint - 4), verificaint));
            if(tipo_campo.find("INT:")==string::npos){
                cout << "Somente é possível indexar um campo inteiro" << endl;
                return;
            }
            
            cout <<"dps\n";
            metadados_index.open("metadados_index.dat", ios :: out | ios::app);
            aux_index = tabela + "_" + campo + "_H";
            metadados_index << aux_index;
            metadados_index.close();
            cout<<"dpsdps\n";            

            while( (linha[j] != ('\n')) && (linha[j] != ('\0'))  ) {
                    endereco_resultado++;
                    j++;
                }
            endereco_resultado++; // para pular pra proxima linha
            j=0;
            
            /*int tabelahash[500][2];
            for(int l=0;l<500;l++){
                tabelahash[l][0] = -1;
                tabelahash[l][1] = -1; 
                }
            */
            std::vector<int> valores;
            std::vector<int> enderecos;
            int tam_vector = 0;
            if (cont_barras > 0) {
                doWhile = 1 ; 
                do {
                    getline(arquivo, linha);
                    //cout << linha <<"<linha"<< endl;
                    while (contador < cont_barras) {
                        
                        while (linha[j] != (';') && (linha[j] != ('\n')) && (linha[j] != ('\0'))) {
                            aux = aux + linha[j];
                            j++;
                        }
                        contador++;
                        if (linha[j] == (';'))
                            j++;
                        if (contador < cont_barras) // quando cont == cont_barras, aux vai ter o valor do campo certo . 
                            aux = "";               //ex: Se cont_barras for 3, o campo que vc quer é CERTIF, entao se 
                    }                               // contador for 1, aux vai ter o valor do CODIGO, 2 vai ser NOME 
                    
                    if( aux != "" ){
                        /*cout<< aux << "_" << endereco_resultado<< "\n";
                        tam_vector ++;
                        */
                        blablabla = stoi(aux);
                        valores.push_back(blablabla);
                        enderecos.push_back(endereco_resultado);
                        tam_vector++;
                    }     


                    

                    while ( (linha[j] != ('\n')) && (linha[j] != ('\0')) ) {
                            j++;
                        }

                    if (arquivo.eof()) {
                        doWhile = 0 ;
                    }
                    contador = 0;
                    aux = "";
                    endereco_resultado = endereco_resultado + j + 1  ; 
                    j = 0;
                    
                }while(doWhile); //termina while1
                
                for (int xxx = 0; xxx < tam_vector ; xxx++)
                        cout << valores[xxx] << "_" << enderecos[xxx]<< "\n";

                caminho_arqv = "Hash/" + tabela + "_" + campo + "_H"; 

                //arquivo_hash.open("Hash/" + caminho_arqv, ios::app); //cria arquivo da hash
                cout << caminho_arqv << "\n" << tam_vector << "\n";
                inicializa( caminho_arqv, tam_vector);
                cout <<"chegou aqui " << caminho_arqv << "\n\n";

                for(int l = 0; l < tam_vector; l++){
                    insere( caminho_arqv, valores[l], enderecos[l]);
                }

                arquivo_hash.close();
                arquivo.close();
                
                cout << "Indice usando hashing criado na tabela " << tabela << " usando a chave " << campo << endl;
            }
            }else{
                cout << "já existe hash para o campo escolhido" << endl;
                return;
        }
    }
}

bool comandoBR(string input) { 
    //cout<<"\n__comando BR__\n";
    bool achou = 0 ;
    int i = 3, j = 0, k = 0 , contador = 0,cont_barras, endereco_resultado = 0, doWhile;
    char modo;
    string tabela, campo, valor, linha, aux, nome_arqv, resultado ,linha_buscas;
    ifstream arquivo;
    //cout << input[i] <<"< inputI\n ";
    if( input[i] == 'U'){
        modo = 'U';
        i++;
    }else{
        modo = 'N';
        i++;
    }

    //cout << modo <<"< modo\n ";
    ignorarEspacos(i, input);
    nomeTabela(i, tabela, input);

    ignorarEspacos(i, input);
    if (pegaCampo(i, campo, input))
        cout << "pegou o campo ->  " << campo <<endl;
    if (pegaValor(i, valor, input))
        cout << "pegou o valor ->  " << valor <<endl<<endl;


     if(verificaHash(tabela,campo)){
        string caminho_arqv = "Hash/" + tabela + "_" + campo + "_H";
        int valorInt = stoi(valor);
        busca(caminho_arqv, valorInt, tabela, modo);
        cout<< "------------------------------------------------------busca por indice\n\n";

        return 1;
     }
    //ofstream buscas("buscas.dat",ios:: out );
    if (verificaSeExiste(tabela, nome_arqv)) {
        Resultado *comeco = NULL;
        nome_arqv = "Tabelas/" + nome_arqv;
        cont_barras = verificaCampo(nome_arqv, campo); //  o nome do arquivo é 1,2,...,x.dat
  

        arquivo.open(nome_arqv, ios:: in ); //  o nome do arquivo  é 1,2,...,x.dat
        getline(arquivo, linha); // pula a linha do indice , por ex"INT:CODIGO;STR:NOME;BIN:CERTIF"
        while( (linha[j] != ('\n')) && (linha[j] != ('\0'))  ) {
                endereco_resultado++;
                j++;
            }
        endereco_resultado++; // para pular pra proxima linha
        j=0;
        if (cont_barras > 0) {
            doWhile = 1 ; 
            do {
                getline(arquivo, linha);
                //cout << linha <<"<linha"<< endl;
                while (contador < cont_barras) {
                    
                    while (linha[j] != (';') && (linha[j] != ('\n')) && (linha[j] != ('\0'))) {
                        aux = aux + linha[j];
                        j++;
                    }
                    contador++;
                    if (linha[j] == (';'))
                        j++;
                    if (contador < cont_barras) // quando cont == cont_barras, aux vai ter o valor do campo certo . 
                        aux = "";               //ex: Se cont_barras for 3, o campo que vc quer é CERTIF, entao se 
                }                               // contador for 1, aux vai ter o valor do CODIGO, 2 vai ser NOME 
                
                if( aux != "" )
                    cout << aux << "  " << endereco_resultado<<endl;

                if ((aux == valor) && linha[0] != ('*')) {
                    insereResultadoLista( &comeco ,endereco_resultado);                   
                    achou = 1;
                    if(modo == 'U')
                        doWhile = 0;
                }
                
                while ( (linha[j] != ('\n')) && (linha[j] != ('\0')) ) {
                        j++;
                    }

                if (arquivo.eof()) {
                    doWhile = 0 ;
                }
                contador = 0;
                aux = "";
                endereco_resultado = endereco_resultado + j + 1  ; 
                j = 0;
                
            }while(doWhile); //termina while1


            if(achou)
            {
                    insereTabelaLista(&inicio, tabela);
                    Atualiza(&inicio, tabela, &comeco);

                    //====================================================
                    
                            
            }
            //cout<< resultado << "<escreveu isso"<< endl;
            //cout << nome_arqv <<"<nome_arqv\n";
            return achou; // achou
        }
        //cout << "  nao existe o campo procurado" << endl;
        return false; // nao existe o campo
    }
    //cout << "  nao existe a tabela procurada" << endl;
    //cout << nome_arqv <<"<nome_arqv\n";
    return false; // nao existe a tabela
}


void comandoCI(string input) {
    int i = 2, nomeVal;
    string tabela, chave, nome_arqv;
    fstream arquivo_arvore;
    ignorarEspacos(i, input);
    input[i] = toupper(input[i]);
    if (input[i] == 'A') {
        i++;
        ignorarEspacos(i, input);
        nomeVal = nomeTabela(i, tabela, input);
        if (nomeVal == 0) { // Verificacao se o nome da tabela é valido
            if (verificaSeExiste(tabela, nome_arqv)){
                if(verificaArvore(nome_arqv)){
                    cout << "Já existe uma indexacao usando arvore na tabela especificada" << endl;
                    return;
                }
            }
            arquivo_arvore.open("Arvore /"+nome_arqv, ios::app);
            ignorarEspacos(i, input);
            pegaChave(i, chave, input);
            arquivo_arvore.close();
            cout << "Indice estruturado como árvores de multiplos caminhos criado na tabela " << tabela << " usando a chave " << chave << endl;
        }

    }
    if (input[i] == 'H') {
        i++;
        ignorarEspacos(i, input);
        nomeVal = nomeTabela(i, tabela, input);
        if (nomeVal == 0) { // Verificacao se o nome da tabela é valido
            cout << "aoba";
            comandoHASH(input);
        }
    }
}

bool interpretador(string input){

    if (verificaVazia(input)== true){ // Se a string estiver vazia, então a ignora e retorna
		return true;
	}


  input[0] = toupper(input[0]);
  input[1] = toupper(input[1]);


  if(input[0] == 'E' and input[1] == 'B'){
    cout << "*****PROGRAMA ENCERRADO*****\n";
    exit(0);
  }else if(input[0] == 'C'){
    if(input[1] == 'T' and input[2] == ' '){ //CT
      comandoCT(input);
      return true;
    }
    else if(input[1] == 'I' and input[2] == ' '){ //CI
      comandoCI(input);
      return true;
    }else{
       cout << "comando não encontrado1, ";
       return true;
     }
  }else if (input[0] == 'R'){
    if(input[1] == 'T' and input[2] == ' '){ //RT
      comandoRT(input);
      return true;
    }
    else if(input[1] == 'I' and input[2] == ' '){ //RI
      comandoRI(input);
      return true;
    }
    else if(input[1] == 'R' and input[2] == ' '){ //RR
      comandoRR(input);
      return true;
    }else {
        cout << "comando não encontrado2, ";
        return true;
    }

  }else if (input[0] == 'A'){
    if(input[1] == 'T' and input[2] == ' '){ //AT
      comandoAT(input);
      return true;
    }
    else if(input[1] == 'R' and input[2] == ' '){ //AR
      comandoAR(input);
      return true;
    }else{
        cout << "comando não encontrado3, ";
        return true;
     }

  }else if (input[0] == 'G' and input[1] == 'I'){ //GI
    comandoGI(input);
    return true;

  }else if (input[0] == 'I' and input[1] == 'R'){ //IR
    comandoIR(input);
    return true;

  }else if (input[0] == 'L' and input[1] == 'T'){ //IR
    comandoLT(input);
    return true;
  }

  else if (input[0] == 'B' and input[1] == 'R' and  input[2] == ' ' and ((input[3] == 'U') || (input[3] == 'N')) and input[4] == ' ' ){ // BR
    comandoBR(input);
    return true;
     }

  else{ // caso tenha erro na sintaxe de digitacao
    cout << "comando não encontrado4, ";
    return true;
  }
}

int main(int argc, char *argv[]){

    verificaMetadados();

  ifstream arquivo;
  string input;
  bool comandos = true;
    if (argc==2){ // passou um arquivo com as instrucoes como argumento
        arquivo.open(argv[1]);
        while (!arquivo.eof())
        {
            getline(arquivo, input);
            interpretador(input);
        }
    }else{ // pelo terminal

      while(comandos){
        getline(cin,input); // pega toda a linha
        comandos = interpretador(input);
      }
    }
  return 0;
}
