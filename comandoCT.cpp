#include "comandoCT.h"
#include "funcoes_comuns.h"

bool comandoCT(string input){ //Cria arquivo
int i = 2,j = 0,tipos, index, nomeVal;
string tabela, linha, ultimo, nome_arquivo, atributo, tabela_cadastrada, buffer;
fstream metadados,disponiveis;
ofstream nova_tabela;


  ignorarEspacos(i, input);
  nomeVal= nomeTabela(i,tabela,input);
  if (nomeVal ==1){ 	// Verificacao se o nome da tabela é valido
  	return false;
  }

  tipos = verificaTipo(i, input);

  if (tipos == 0){

    metadados.open("metadados.dat", ios::out|ios::in|ios::app); //abre em modo leitura, escrita e mantendo as informações já salvas
    //metadados.seekg(0,metadados.beg);

    if(verificaTabela(metadados,tabela, linha)){ //verifica se existe tabela com o nome da que será criada
        cout<< "Existe uma tabela com o nome " << tabela << endl;
        return false;
    }
    metadados.seekg(0,ios::beg);
    //getline(metadados,linha);

    linha = "";
    buffer = "";
    while (!metadados.eof())
    { 
      getline(metadados,buffer);
      if(buffer[0]!='*' || linha[0]=='I') linha = buffer; // atribui a string linha a última tabela lida que não foi excluída
    }
    
    if (linha[0] == 'I'){ //Caso seja o primeiro arquivo o Indice é 0, será incrementado depois
      ultimo = "0";

    }else{
      while(linha[j]!='|'){ //Pega o índice do último arquivo criado

          ultimo = ultimo + linha[j];
          j++;
         }
    }

    index = atoi(ultimo.c_str()); //converte o índice do último arquivo para inteiro e após incrementa
    metadados.close();
      metadados.open("metadados.dat", ios::out|ios::in|ios::app);
    metadados << endl << index+1<< "|" << tabela ;
    metadados.close();
    nome_arquivo = "Tabelas/"+ to_string(index+1) + ".dat"; //Cria o arquivo da tabela onde serão inseridos os dados
    nova_tabela.open(nome_arquivo,ios::out|ios::app);
    

    //Os nomes das colunas com os tipos vão ser pegos para inserir no arquivo da tabela
    i=2;
    ignorarEspacos(i, input);
    tabela = "";
    nomeTabela(i,tabela,input); //aloca o i para encontrar os atributos da tabela
    ignorarEspacos(i, input);
    while (input[i]!='\0')
    {
        atributo = atributo + input[i];
        i++;
    }

    nova_tabela << atributo << endl;
    nova_tabela.close();

    
    nome_arquivo = "Disponiveis/"+ to_string(index+1) + ".dat"; //Cria o arquivo da tabela onde serão inseridos os espaços disponiveis para a inserção
    disponiveis.open(nome_arquivo,ios::out);
    disponiveis.close();
    
    cout << "Tabela " << tabela << " criada\n";
    return true;

  }else{
  	cout<< "Tabela" << tabela << " não criada por erro em tipo digitado\n";
    return false;
  	}


}