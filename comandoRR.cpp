#include "comandoRR.h"
#include "funcoes_comuns.h"

bool comandoRR(string input){
  int i = 2, nomeVal, endereco_int;
  string tabela, linha, endereco_str, nome, index, aux, end_tam;
  fstream buscas, metadados, disponiveis,arq_tabela;

  metadados.open("metadados.dat", ios::in);

  ignorarEspacos(i, input);
  nomeVal = nomeTabela(i,tabela,input);
  
  if (nomeVal ==1){ 	// Verificacao se o nome da tabela é valido
  	return false;
  }
  if(verificaTabela(metadados,tabela, linha)){
    i=0;
    index = "";
    
    do{
      
      index = index + linha[i];
      i++;
    }while(linha[i]=!'|'); //pega o index da tabela
    if(verificaHash(tabela,"")){
			cout << "Não é possível remover um registro em uma tabela já indexada com hash"<<endl;
			return false;
		}
    buscas.open("buscas.dat", ios::in);
    
    while(nome != tabela && !buscas.eof()){ //procura o endereço dos registros a serem invalidados
      
      getline(buscas,linha); 
      i=0;
      nome = "";

      while(linha[i]!='|'){
        nome = nome + linha[i];
        i++;
      }
      
      i++;
    }
     if(tabela==nome){   
       cout << linha << "__linha" << endl;
       
      while(linha[i]!=';'){
        endereco_str= "";
        
        while(linha[i]!=' ' && linha[i]!=';'){
          endereco_str = endereco_str+linha[i];
          i++;
        }
        endereco_int = atoi(endereco_str.c_str());
        
        arq_tabela.open("Tabelas/"+index+".dat", ios::out|ios::in|ios::ate); //abre o arquivo da tabela que irá invalidar o registro
        arq_tabela.seekp(endereco_int,arq_tabela.beg);//posiciona no registro a ser excluído
        arq_tabela << '*'; // invalida registro
        getline(arq_tabela,aux);
        
        disponiveis.open("Disponiveis/"+index+".dat", ios::in|ios::app);
        end_tam = to_string(endereco_int) + "|" + to_string(aux.length()+1) ;
        
        disponiveis << end_tam <<endl;
         
        arq_tabela.close();
        disponiveis.close();
        i++;
      }
      }else{ // não teve busca bem sucedida na tabela
        return false;
      }
      
    
  }else{
    cout << "Tabela não cadastrada " << endl;
  }
}