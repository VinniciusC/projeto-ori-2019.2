
#include "comandoRT.h"
#include "funcoes_comuns.h"

bool comandoRT(string input){
  int i = 2, j=0, teste, nomeval;
  string tabela, linha, index, remover, endereco;
  fstream metadados, arquivo_arvore, arquivo_hash;

    ignorarEspacos(i, input);
    nomeval = nomeTabela(i,tabela,input);
    if(nomeval == 1){
      return false;
    }

    metadados.open("metadados.dat", ios::in|ios::out|ios::ate);

    if(verificaTabela(metadados,tabela,linha)){ //verifica se existe a tabela que será excluída
      metadados.seekg(0,ios::beg );  //caso exista, posicionará o ponteiro no início para fazer a cópia em um novo arquivo e excluir os metadados da tabela desejada

      remover = linha; //salva a linha que deve ser removida

      while(linha[j]!='|'){ //pega o indice para excluir o arquivo da tabela
        index+= linha[j];
        j++;
      }
      
      string endereco = "Tabelas/" + index + ".dat"; //cria endereço do arquivo a ser removido
      remove(endereco.c_str()); //exclui o arquivo da tabela

      endereco = "Hash/"+ index + ".dat";
      arquivo_hash.open(endereco);
      if(arquivo_hash){
          remove(endereco.c_str());
      }

      endereco = "Arvore /"+ index + ".dat";
      arquivo_arvore.open(endereco);
      if(arquivo_arvore){
          remove(endereco.c_str());
      }
      
      endereco = "Disponiveis/" + index + ".dat"; 
      remove(endereco.c_str()) ;
      metadados.seekp(0,ios::beg );
      
      getline(metadados,linha);
      while (linha!=remover) //Posiciona o ponteiro na linha da tabela que será invalidade
      { 
        getline(metadados,linha);
      }
      if(metadados.eof()){ //Se atingiu o final do arquivo, a escrita não funciona mais. 
          metadados.close(); // É necessário fechar o arquivo e abrir novamente
          metadados.open("metadados.dat", ios::in|ios::out|ios::ate);
          metadados.seekp((linha.length()*-1),ios::end); 
          metadados << "*";
      }else{
        metadados.seekp((linha.length()*-1)-1,ios::cur); //Posiciona o ponteiro no caractere de index para invalidar a tabela
        metadados << "*";
      }
      metadados.close();

      cout << "Tabela " << tabela << " excluida\n";
      return true;
    }

  cout << "Não existe a tabela " << tabela << endl;
  return false;
}